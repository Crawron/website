import React from "react"
import styled from "styled-components"

import NavLink from "./NavLink"

const NavBoxStyle = styled.nav`
	display: inline-block;
`

export default function NavBox() {
	return (
		<NavBoxStyle>
			<NavLink href="https://discord.gg/BmM79tw" content="Discord" />
			<NavLink href="https://twitter.com/Crawron" content="Twitter" />
			<NavLink href="https://crawron.tumblr.com" content="Portfolio" />
			{/* <NavLink href="about" content="About" /> */}
		</NavBoxStyle>
	)
}
