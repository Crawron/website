import React from "react"
import styled from "styled-components"

import { GlobalStyle } from "./GlobalStyle"
import { background, foreground } from "./styles"
import logo from "./assets/Logo.svg"

import NavBox from "./NavBox"

const AppStyle = styled.div`
	min-height: 100vh;
	color: ${foreground};
	background-color: ${background};
`

const Positioning = styled.div`
	display: flex;
	flex-flow: column;

	position: absolute;
	bottom: 0;
	margin: 4rem 8rem;
`

const Logo = styled.div`
	width: 2rem;
	height: 2rem;

	background: url(${logo});

	margin-bottom: 1rem;
`

export default function App() {
	return (
		<AppStyle className="App">
			<GlobalStyle />
			<Positioning>
				<Logo />
				<NavBox />
			</Positioning>
		</AppStyle>
	)
}
