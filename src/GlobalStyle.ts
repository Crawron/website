import { createGlobalStyle } from "styled-components"

export const GlobalStyle = createGlobalStyle`
	body {
		margin: 0;
		font-family: "Work Sans", sans-serif;
	}
`
