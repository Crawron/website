import React from "react"
import styled from "styled-components"

const StyledA = styled.a`
	display: block;

	padding-top: 1rem;

	font-size: 24px;
	letter-spacing: 0.1em;

	text-transform: uppercase;
	text-decoration: none;
	color: white;
`

interface NavLinkProps {
	content: string
	href: string
	sameTab?: boolean
}

export default function NavLink(props: NavLinkProps) {
	return (
		<StyledA href={props.href} target={props.sameTab ? "" : "_blank"}>
			<div>{props.content}</div>
		</StyledA>
	)
}
